﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteOrient : MonoBehaviour {

    public SpriteRenderer player;

	void Start () {
        player = GetComponent<SpriteRenderer>();
	}
	
	void Update () {
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            {
                player.flipX = true;
            }
            if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            {
                player.flipX = false;
            }
    }
}
