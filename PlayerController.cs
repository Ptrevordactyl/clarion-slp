using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed = 7;
    public float jumpForce = 10;
    public float fireDelay = .5f;
    public int extraJumpCount;

    public Rigidbody2D playerBody;
    public Collider2D playerCollider;
    public GameObject projectile;

    private float fireDelayTimer;
    private int jumpCount;
    private bool isGrounded;

    private void Start()
    {
        playerBody = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<BoxCollider2D>();
    }

    void Update ()
    {
        ShootCheck();
        JumpCheck();
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    void JumpCheck()
    {
        isGrounded = IsGrounded(playerCollider);
        if(isGrounded)
        {
            jumpCount = extraJumpCount; //reset jump count
        }
        //jump
        if (Input.GetKeyDown(KeyCode.Space)) //if player presses space down
        {
            if(isGrounded) //if player is grounded
            {
                playerBody.velocity = new Vector2(playerBody.velocity.x, Vector2.up.y * jumpForce); //set player's vertical velocity without affecting horizontal
            }
            else if(jumpCount > 0) //if player is in the air but still has an extra jump
            {
                jumpCount--; //decrement jump count
                playerBody.velocity = new Vector2(playerBody.velocity.x, Vector2.up.y * jumpForce); //set player's vertical velocity without affecting horizontal
            }
        }
    }

    void ShootCheck()
    {
        Vector2 lookDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position; //get vector between mouse and player position
        float lookAngle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg; //get angle of lookDirection vector
        if (Input.GetMouseButtonDown(0) && fireDelayTimer <= 0) //if player presses left mouse button and fire delay timer is done
        {
            fireDelayTimer = fireDelay; //reset timer
            Instantiate(projectile, transform.position, Quaternion.Euler(0, 0, lookAngle - 90)); //create projectile, set it's initial position and angle
        }
        if (fireDelayTimer > 0) //timer hasn't run out
        {
            fireDelayTimer -= Time.deltaTime; //reduce timer
        }
    }

    void MovePlayer()
    {
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), 0); //horizontal input from player
        Vector2 velocity = input.normalized * speed;

        playerBody.velocity = new Vector2(velocity.x, playerBody.velocity.y); //set the player's horizontal velocity
    }

    public static bool IsGrounded(Collider2D col)
    {
        RaycastHit2D boxCast = Physics2D.BoxCast(col.bounds.center, col.bounds.size, 0, Vector2.down, .01f, 512); //cast a box from center of collider down over distance equal to half of collider's size, only hits if on layer 9 (ground)
        if (boxCast.collider != null && 0 < boxCast.distance )
        {
            return true;
        }
        return false;
    }
}
