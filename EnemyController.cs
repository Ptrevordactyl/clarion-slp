﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public float speed = 5;
    public float jumpForce = 16;
    public float maxSpeed = 1;
    public float jumpTriggerDistance = 3;
    public float jumpTriggerHeight = 3;

    public Collider2D bodyCollider;
    public Rigidbody2D enemyBody;

	void FixedUpdate ()
    {
        MoveEnemy();
        JumpCheck();
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.CompareTag("player")) //if enemy touches player
        {
            Destroy(GameObject.FindGameObjectWithTag("player")); //destroy player
        }
    }

    void MoveEnemy()
    {
        if (GameObject.FindGameObjectWithTag("player") != null) //check that player exists
        {
            Vector2 playerPosition = GameObject.FindGameObjectWithTag("player").transform.position;
            if (transform.position.x < playerPosition.x && enemyBody.velocity.x < maxSpeed) //player is to the right
            {
                enemyBody.AddForce(Vector2.right * speed); //move enemy to the right
            }
            if (transform.position.x > playerPosition.x && enemyBody.velocity.x > -maxSpeed) //player is to the left
            {
                enemyBody.AddForce(Vector2.left * speed); //move enemy to the left
            }
        }
        else //player was destroyed
        {
            enemyBody.velocity = new Vector2(0, enemyBody.velocity.y); //stop horizontal movement
        }
    }

    void JumpCheck()
    {
        //jumping
        if (GameObject.FindGameObjectWithTag("player") != null) //check that player exists
        {
            Vector2 playerPosition = GameObject.FindGameObjectWithTag("player").transform.position; //find player's position
            if (((playerPosition.y - transform.position.y > jumpTriggerHeight && playerPosition.x - transform.position.x<jumpTriggerDistance) || enemyBody.velocity == Vector2.zero) && PlayerController.IsGrounded(bodyCollider)) //if player is above enemy AND player is close enough OR enemy is stuck AND enemy is grounded
            {
                enemyBody.velocity = new Vector2(enemyBody.velocity.x, Vector2.up.y * jumpForce); //add a force to the enemy to make it jump
            }
        }
    }
}
