﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteHandler : MonoBehaviour {

    public SpriteRenderer sprite;
    public Transform enemy;
    private Transform player;

	void Start () {
        sprite = GetComponent<SpriteRenderer>();
        if(GameObject.FindGameObjectWithTag("player") != null) //check that the player exists, it won't exist if player dies
        {
            player = GameObject.FindGameObjectWithTag("player").transform; //keep track of player's transform
        }
	}
	
	void Update () {
        if(player != null)
        {
            if (enemy.position.x < player.position.x) //enemy is left of player
            {
                sprite.flipX = false; //face right ; towards player
            }
            if (enemy.position.x > player.position.x) //enemy is right of player
            {
                sprite.flipX = true; //face left ; towards player
            }
        }
	}
}
