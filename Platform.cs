﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
	
    public float waitTime = .2f;
    private float waitTimer;
    public Collider2D playerCol;

    private void Start()
    {
        waitTimer = waitTime; //initialize timer
    }

    void Update()
    {
        if((Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.S))) //if player releases down
        {
            waitTimer = waitTime; //reset timer
        }

        if ((Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) && playerCol != null && isTouchingPlayer()) //if player is holding down and is on the platform
        {
            if (waitTimer <= 0)
            {
                GetComponent<PlatformEffector2D>().rotationalOffset = 180; //set platformeffector to down, lets player fall through
                waitTimer = waitTime;
            }
            else
            {
                waitTimer -= Time.deltaTime; //reduce timer
            }
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<PlatformEffector2D>().rotationalOffset = 0; //reset platformeffector top up
        }
    }

    private bool isTouchingPlayer()
    {
        Collider2D[] contacts = new Collider2D[50];
        Physics2D.GetContacts(GetComponent<Collider2D>(), contacts); //get all colliders in contact with the platform
        foreach(Collider2D col in contacts) //check every element of contacts array
        {
            if(col == playerCol) //if the players collider is in contact with platform, return true
            {
                return true;
            }
        }
        return false;
    }
}