﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {

    public GameObject player;
    public GameObject GameOverHandler;
    public GameObject ExistingGameOverHandler;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("player");
	}
	
	// Update is called once per frame
	void Update () {
		if(player == null && ExistingGameOverHandler == null)
        {
            ExistingGameOverHandler = Instantiate(GameOverHandler, transform);
        }
	}
}
