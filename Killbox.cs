﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killbox : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D col)
    {
        Destroy(col.gameObject); //destroy any object that passes through
    }
}
