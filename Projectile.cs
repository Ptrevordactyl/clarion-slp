﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 10;
    public float knockback = 10;
    private Vector3 moveDirection;
    public Rigidbody2D body;
    public GameObject projectile;

    // Use this for initialization
    void Start()
    {
        body.velocity = transform.up * speed; //set velocity in the same direction as projectile's instantiated angle
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Rigidbody2D>() != null) //make sure rigidbody exists before modifying it
        {
            col.GetComponent<Rigidbody2D>().AddForce(body.velocity.normalized * knockback, ForceMode2D.Impulse); //add knockback to the thing hit by projectile
        }
        if(col.tag != "platform" && col.tag != "spawner") //don't want projectile to collide with platforms or spawners
        {
            Destroy(projectile); //destroy projectile after it hits something
        }
    }
}
