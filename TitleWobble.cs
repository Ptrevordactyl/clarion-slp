﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleWobble : MonoBehaviour
{

    public RectTransform title;
    private bool rotateLeft = true;
    private bool grow = true;
    private float titleScaleX;

    void Start()
    {
        title = GetComponent<RectTransform>();
        titleScaleX = title.localScale.x; //title's x scale was adjusted manually, keep track of how much
    }

    void Update()
    {
        Wobble();
        Pulsate();
    }

    void Wobble()
    {
        if (rotateLeft)
        {
            title.eulerAngles += new Vector3(0, 0, 0.5f); //increase z rotation by 0.5 degrees
            if (title.eulerAngles.z > 15 && title.eulerAngles.z < 16) //continue rotating until rotation is greater than 15 degrees
            {
                rotateLeft = false; //switch rotation direction
            }
        }
        else
        {
            title.eulerAngles -= new Vector3(0, 0, 0.5f); //decrease z rotation by 0.5 degrees
            if (title.eulerAngles.z < 345 && title.eulerAngles.z > 344) //continue rotating until rotation is less than -15 degrees
            {
                rotateLeft = true; //switch rotation direction
            }
        }
    }

    void Pulsate()
    {
        if (grow)
        {
            title.localScale += new Vector3(titleScaleX * 0.005f, 0.005f, 0.005f); //increase scale by .5%
            if(title.localScale.z >= 1.2f) //grow until scale is bigger than 1.2x initial size
            {
                grow = false; //switch to decreasing
            }
        }
        else
        {
            title.localScale -= new Vector3(titleScaleX * 0.005f, 0.005f, 0.005f); //decrease scale by .5%
            if (title.localScale.z <= 0.8f) //shrink until less than .8x initial size
            {
                grow = true; //switch to increasing
            }
        }
    }
}
