﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterSpawner : MonoBehaviour {

    public int enemyCount = 0;
    public int maxEnemyCount = 5;

    private GameObject[] spawnedEnemies;

    private void Update()
    {
        spawnedEnemies = GameObject.FindGameObjectsWithTag("enemy");
        enemyCount = spawnedEnemies.Length;
        if(enemyCount > maxEnemyCount) //if there are too many enemies by accident
        {
            Destroy(spawnedEnemies[spawnedEnemies.Length-1]); //delete the most recent one
        }
    }
}
