﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public float spawnDelay = 10;
    public float spawnOffset = 1;
    private float spawnTimer;
    private bool playerInBound;

    public GameObject enemy;
    public GameObject masterSpawner;

    private void Start()
    {
        spawnTimer = spawnDelay + spawnOffset; //initialize timer
    }

    void Update () {
		if(spawnTimer > 0) //if timer hasn't reached zero, don't spawn anything this update
        {
            spawnTimer -= Time.deltaTime;  //count timer down
        }
        else
        {
            spawnTimer = spawnDelay; //reset timer
            if ((masterSpawner.GetComponent<MasterSpawner>().enemyCount < masterSpawner.GetComponent<MasterSpawner>().maxEnemyCount) && !playerInBound) //if timer has reached zero and enemy limit isn't reached
            {
                Instantiate(enemy, transform.position, Quaternion.identity); //spawn enemy, add to array of existing enemies
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D col) //when player gets too close to spawner
    {
        if (col.tag == "player") //make sure that the collider in range of spawner is the player
        {
            playerInBound = true; //tell spawner that player is too close
        }
    }

    private void OnTriggerExit2D(Collider2D col) //when player isn't too close to spawner anymore
    {
        if (col.tag == "player") //make sure that the collider in range of spawner is the player
        {
            playerInBound = false; //tell spawner that player isn't too close anymore
        }
    }
}